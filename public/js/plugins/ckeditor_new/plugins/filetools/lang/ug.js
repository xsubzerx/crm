﻿/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
CKEDITOR.plugins.setLang( 'filetools', 'ug', {
	loadError: 'ھۆججەت ئوقۇشتا خاتالىق كۆرۈلدى',
	networkError: 'ھۆججەت يۈكلەشتە تور خاتالىقى كۆرۈلدى.',
	httpError404: 'ھۆججەت يۈكلىگەندە HTTP خاتالىقى كۆرۈلدى (404: ھۆججەت تېپىلمىدى).',
	httpError403: 'HTTP errors occurred during file upload (403: Forbidden).', // MISSING
	httpError: 'HTTP errors occurred during file upload (errors status: %1).', // MISSING
	noUrlError: 'Upload URL is not defined.', // MISSING
	responseError: 'Incorrect server response.' // MISSING
} );
