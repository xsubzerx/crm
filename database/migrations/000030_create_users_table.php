<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('fax')->nullable();
            $table->integer('department_id')->default(0);
            $table->string('job')->nullable();
            $table->integer('manager')->default(0);
            $table->string('image')->nullable();
            $table->integer('role_id');
            $table->tinyInteger('status')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });

        \DB::table('users')->insert([
            'id'        => 1,
            'name'      => 'Administrator',
            'email'     => 'admin@crm.com',
            'password'  => \Illuminate\Support\Facades\Hash::make('123456789'),
            'role_id'   => 1,
            'status'    => 1
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
