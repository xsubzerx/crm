<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent')->default(0);
            $table->string('name');
            $table->string('address')->nullable();
            $table->integer('country_id');
            $table->integer('city_id');
            $table->string('zip')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->unique();
            $table->string('fax')->nullable();
            $table->string('email')->unique();
            $table->string('website')->nullable();
            $table->string('image')->nullable();
            $table->enum('title', ['none', 'mr', 'ms', 'mrs', 'dr', 'prof'])->default('none');
            $table->enum('type', ['individual', 'company'])->default('individual');
            $table->string('position')->nullable(); //individual only
            $table->string('department')->nullable(); //individual only
            $table->string('industry')->nullable(); //company only
            $table->integer('employees')->nullable(); //company only
            $table->integer('related')->default(0);
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
