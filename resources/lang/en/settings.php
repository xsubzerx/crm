<?php

return [
    'settings'  => 'Settings',
    'site-name' => 'Company Name',
    'site-email'=> 'Global E-Mail Address',
    'logo'      => 'Company Logo',
];
