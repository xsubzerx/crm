<?php

return array(
	'roles' 		    => 'Roles',
    'role'			    => 'Role',
	'roles-list' 	    => 'Roles List',
	'view-roles'	    => 'View Roles',
	'name'		        => 'Role Name',
	'description'	    => 'Description',
	'privileges'	    => 'Privileges',
    'actions'           => [
        'action'    => 'Action',
        'view'      => 'View',
        'new'       => 'Create',
        'edit'      => 'Update',
        'destroy'   => 'Delete',
    ]
);
