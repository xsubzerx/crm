<?php

return [
    'countries'        => 'Countries',
    'view-countries'   => 'View Countries',
    'country-title'    => 'Country Name',
    'create-country'   => 'Create New Country',
    'update-country'   => 'Update Country',
];
