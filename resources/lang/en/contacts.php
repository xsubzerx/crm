<?php

return [
    'contacts'          => 'Contacts',
    'contact'           => 'Contact',
    'customers'         => 'Customers',
    'customer'          => 'Customer',
    'leads'             => 'Leads',
    'lead'              => 'Lead',
    'opportunities'     => 'Opportunities',
    'opportunity'       => 'Opportunity',
    'create'            => [
        'contact'    => 'Create New Contact',
        'lead'       => 'Create New Lead',
        'opportunity'=> 'Create New Opportunity'
    ],
    'update'            => [
        'contact'    => 'Update Contact Data',
        'lead'       => 'Update Lead Data',
        'opportunity'=> 'Update Opportunity Data'
    ],
    'delete'            => [
        'contact'    => 'Delete Contact',
        'lead'       => 'Delete Lead',
        'opportunity'=> 'Delete Opportunity'
    ],
    'view'              => [
        'contacts'       => 'View Contacts',
        'leads'          => 'View Leads',
        'opportunities'  => 'View Opportunities',
        'customers'      => 'View Customers'
    ],

    'name'              => 'Name',
    'address'           => 'Address',
    'zip'               => 'Zip Code',
    'phone'             => 'Phone',
    'mobile'            => 'Mobile',
    'fax'               => 'Fax',
    'email'             => 'E-Mail',
    'website'           => 'Website URL',
    'image'             => 'Photo',
    'parent'            => 'Parent',
    'department'        => 'Department',
    'createdBy'         => 'Created By',
    'related'           => 'Related To',
    'child-of'          => 'Child of',
    'created-by'        => 'Created By',
    'updated-by'        => 'Updated By',
    'contact-search'    => 'Search on Contact',
    'lead-source'       => 'Lead Source',
    'titles'            => [
        'title' => 'Title',
        'none'  => 'None',
        'mr'    => 'Mr.',
        'ms'    => 'Ms.',
        'mrs'   => 'Mrs.',
        'dr'    => 'Dr.',
        'prof'  => 'Prof.'
    ],
    'types'             => [
        'type' => 'Type', 'individual' => 'Individual', 'company' => 'Company'
    ],
    'position'          => 'Position',
    'industry'          => 'Industry',
    'employees'         => 'No. of Employees',

];