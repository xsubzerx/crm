<?php

return [
    'departments' 		=> 'Departments',
    'department'		=> 'Department',
    'departments-list' 	=> 'Departments List',
    'create'            => 'Create New Department',
    'update'	        => 'Update Department',
    'view-departments'  => 'View Departments',
    'title'             => 'Department Title',
    'select-department' => '-- Select Department -- ',
];