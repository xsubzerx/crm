<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'            => 'These credentials do not match our records.',
    'throttle'          => 'Too many login attempts. Please try again in :seconds seconds.',
    'register'          => 'Register',
    'register-now'      => 'Register Now!',
    'password'          => 'Password',
    'email'             => 'E-Mail',
    'remail'            => 'E-Mail Confirm',
    'terms'             => 'I accepted the user agreement.',
    'welcome'           => 'Welcome back',
    'signin'            => 'Sign In ',
    'forgot-password'   => 'Forget your Password?',
    'remember'          => 'Remember Me',
    'don-have-account'  => 'Don\'t have an account yet?',
    'repass'            => 'Re-Enter Password',
    'have-account'      => 'Already have an account?',
    'login-facebook'    => 'Sign In by your Facebook Account',

];
