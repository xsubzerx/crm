<?php

return [
    'cities'        => 'Citties',
    'city'          => 'City',
    'view-cities'   => 'View Cities',
    'city-title'    => 'City Name',
    'create-city'   => 'Create New City',
    'update-city'   => 'Update City',
    'select-city'   => 'Select City',
    'all-cities'    => 'All Cities',
];
