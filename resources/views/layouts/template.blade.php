<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    @include('layouts.partials._head')
    <body class="pace-done skin-black fixed">
        @include('layouts.partials._header')

        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            @include('layouts.partials._sidebar')
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>@yield('title')</h1>
                    <ol class="breadcrumb">
                        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i>{{ trans('common.cpanel') }}</a></li>
                        @if(isset($breadcrumb) && count($breadcrumb) > 0)
                            @foreach($breadcrumb as $val)
                                <li><a href="{{$val['link']}}"><i class="fa {{$val['icon']}}"></i>{{ $val['label'] }}</a></li>
                            @endforeach
                        @endif
                        <li class="active">@yield('title')</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    @yield('content')
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- jQuery 2.0.2 -->
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <!-- Bootstrap -->
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <!-- AdminLTE App -->
        <script src="{{ asset('js/AdminLTE/app.js') }}"></script>

        @yield('scripts')
    </body>
</html>