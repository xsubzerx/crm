@if(isset($cities) && count($cities))
    <option value="0">{{ trans('cities.select-city') }}</option>
    @foreach($cities as $city)
        <option value="{{$city->id}}" @if($currCity == $city->id) selected="selected" @endif>{{ $city->name }}</option>
    @endforeach
@else
    <option value="0" disabled="disabled">{{ trans('countries.select-country') }}</option>
@endif