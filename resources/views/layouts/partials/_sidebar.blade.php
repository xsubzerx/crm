<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('img/avatar3.png') }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Hello, {{ str_limit(Auth::user()->name, 10) }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <?php $direction = (\App::getLocale() == 'ar') ? 'left' : 'right'; ?>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li @if(Request::is('home')) class="active" @endif><a href="{{ route('home') }}" ><i class="fa fa-dashboard"></i> <span>{{ trans('common.cpanel') }}</span></a></li>
            <li @if(Request::is('settings')) class="active" @endif><a href="{{ route('settings') }}"><i class="fa fa-cogs"></i><span>{{ trans('settings.settings') }}</span></a></li>
            <li class="treeview @if(Request::is('contacts', 'contacts/*', 'customers', 'customers/*', 'leads', 'opportunities', 'dead', 'lost')) active @endif">
                <a href="#"><i class="fa fa-users"></i><span>{{ trans('contacts.contacts') }}</span><i class="fa fa-angle-{{$direction}} pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(Request::is('contacts', 'contacts/*')) class="active" @endif><a href="{{ route('contacts.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('contacts.view.contacts') }}</a></li>
                    <li @if(Request::is('leads')) class="active" @endif><a href="{{ route('leads.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('contacts.view.leads') }}</a></li>
                    <li @if(Request::is('opportunities')) class="active" @endif><a href="{{ route('opportunities.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('contacts.view.opportunities') }}</a></li>
                    <li @if(Request::is('dead')) class="active" @endif><a href="{{ route('dead.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('contacts.view.dead') }}</a></li>
                    <li @if(Request::is('lost')) class="active" @endif><a href="{{ route('lost.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('contacts.view.lost') }}</a></li>
                    <li @if(Request::is('customers', 'customers/*')) class="active" @endif><a href="{{ route('customers.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('contacts.view.customers') }}</a></li>
                </ul>
            </li>
            <li class="treeview @if(Request::is('users', 'users/*', 'departments', 'departments/*', 'roles', 'roles/*')) active @endif">
                <a href="#"><i class="fa fa-users"></i><span>{{ trans('users.users') }}</span><i class="fa fa-angle-{{$direction}} pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(Request::is('users/create')) class="active" @endif><a href="{{ route('users.create') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('users.create') }}</a></li>
                    <li @if(Request::is('users', 'users/*')) class="active" @endif><a href="{{ route('users.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('users.view-users') }}</a></li>
                    <li @if(Request::is('roles', 'roles/*')) class="active" @endif><a href="{{ route('roles.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('roles.view-roles') }}</a></li>
                    <li @if(Request::is('departments', 'departments/*')) class="active" @endif><a href="{{ route('departments.index') }}"><i class="fa fa-angle-double-{{$direction}}"></i> {{ trans('departments.view-departments') }}</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" ><i class="fa fa-sign-out"></i> <span>{{ trans('users.logout') }}</span></a>
                {{ Form::open(['url' => route('logout'), 'method' => 'POST', 'id' => 'logout-form', 'class' => 'hide']) }}{{ Form::close() }}
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>