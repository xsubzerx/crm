@extends('layouts.template')

@section('title', $title)

@section('content')

    {{ (isset($userData)) ? Form::model($userData, ['url' => route('users.update', [$userData->id]), 'method' => 'PUT', 'files'=> true]) : Form::open(['url' => route('users.create'), 'files'=> true]) }}
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        @if(Session::has('msg'))
                            {!! Session::get('msg') !!}
                        @endif
                            @if(!empty($errors->all()))
                            <ul class="callout callout-danger">
                                @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                            </ul>
                        @endif
                    </div>
                    <div class="box-body">
                        <div class="form-group col-md-12">
                            {{ Form::label('name', trans('users.fullname').':') }}
                            {{ Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('users.fullname')]) }}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('email', trans('users.email').':') }}
                            @if(isset($userData->email))
                                <br/> {{ $userData->email }}
                            @else
                                {{ Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => trans('users.email')]) }}
                            @endif
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('password', trans('users.password').':') }}
                            {{ Form::password('password', ['class' => 'form-control', 'placeholder' => trans('users.password')]) }}
                        </div>
                        <div class="form-group col-md-4">
                            {{ Form::label('mobile', trans('users.mobile').':') }}
                            {{ Form::text('mobile', old('mobile'), ['class' => 'form-control', 'placeholder' => trans('users.mobile')]) }}
                        </div>
                        <div class="form-group col-md-4">
                            {{ Form::label('phone', trans('users.phone').':') }}
                            {{ Form::text('phone', old('phone'), ['class' => 'form-control', 'placeholder' => trans('users.phone')]) }}
                        </div>
                        <div class="form-group col-md-4">
                            {{ Form::label('fax', trans('users.fax').':') }}
                            {{ Form::text('fax', old('fax'), ['class' => 'form-control', 'placeholder' => trans('users.fax')]) }}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('department_id', trans('users.department').':') }}
                            {{ Form::select('department_id', $departments, old('department_id'), ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('job', trans('users.job').':') }}
                            {{ Form::text('job', old('job'), ['class' => 'form-control', 'placeholder' => trans('users.job')]) }}
                        </div>
                        <div class="form-group col-md-6">
                                {{ Form::label('image', trans('users.image').':') }}
                                {{ Form::file('image', array('class' => 'form-control', 'placeholder' => trans('users.image'))) }}
                        </div>
                        <div class="form-group col-md-6">
                            <img src="{{ (isset($userData) && $userData->image) ? route('image.small', ['users',$userData->image]) : asset('img/no-image-found.jpg') }}" class="center-block" height="100" />
                        </div>
                        <div class="form-group col-md-4">
                            {{ Form::label('role_id', trans('users.privileges').':') }}
                            {{ Form::select('role_id', $roles, old('role_id'), ['id' => 'role_id', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group col-md-4">
                            {{ Form::label('manager', trans('users.manager').':') }}
                            {{ Form::select('manager', $managers, old('manager'), ['class' => 'form-control']) }}
                        </div>
                        <div class="form-group col-md-4">
                            {{ Form::label('status', trans('common.status').':') }}
                            {{ Form::select('status', ['1' => trans('common.active'),'0' => trans('common.deactive')], old('status'), ['id' => 'status', 'class' => 'form-control']) }}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="box-footer">
                        {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
                    </div>
                </div>
            </div>
        </div>
    {{ Form::close() }}

@stop