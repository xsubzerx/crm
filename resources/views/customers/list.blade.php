@extends('layouts.template')

@section('title'){{ $title }} @stop

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="text-right">
                <a href="{{ route('customers.create') }}" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-plus"></i> {{ trans('common.create-new', ['item' => trans('contacts.lead')]) }}</a>
            </div>
            <div class="box-header">
                @if(Session::has('msg'))
                    {!! Session::get('msg') !!}
                @endif
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="example1" class="table table-bordered table-striped text-center">
                    <thead>
                        <tr>
                            <th>{{ trans('contacts.name') }}</th>
                            <th>{{ trans('contacts.mobile') }}</th>
                            <th>{{ trans('contacts.email') }}</th>
                            <th>{{ trans('contacts.image') }}</th>
                            <th>{{ trans('contacts.types.type') }}</th>
                            <th>{{ trans('common.preview') }}</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>{{ trans('contacts.name') }}</th>
                            <th>{{ trans('contacts.mobile') }}</th>
                            <th>{{ trans('contacts.email') }}</th>
                            <th>{{ trans('contacts.image') }}</th>
                            <th>{{ trans('contacts.types.type') }}</th>
                            <th>{{ trans('common.preview') }}</th>
                        </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

@stop

@section('scripts')
    {{ Html::style('css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('js/plugins/datatables/jquery.dataTables.min.js') }}
    {{ Html::script('js/plugins/datatables/dataTables.bootstrap.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable({
                processing: true,
                serverSide: true,
                ajax: "{{route(Route::currentRouteName())}}",
                language: {
                    "emptyTable": "{{trans('common.no-data-found')}}"
                }
            });
        });
    </script>
@stop