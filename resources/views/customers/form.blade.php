@extends('layouts.template')

@section('title', trans('common.create-new', ['item' => trans('contacts.lead')]))

@section('content')

    {{ Form::open(['url' => route('customers.store')]) }}
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        @if(Session::has('msg'))
                            {!! Session::get('msg') !!}
                        @endif
                        @if(!empty($errors->all()))
                            <ul class="callout callout-danger">
                                @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                            </ul>
                        @endif
                    </div>
                    <div class="box-body">
                        <div class="form-group col-md-12">
                            {{ Form::label('contact', trans('contacts.contact').':') }}
                            {{ Form::select('contact_id', $contacts, old('contact_id'), ['id' => 'contact', 'class' => 'form-control js-example-basic-single', 'required']) }}
                        </div>
                        <div class="form-group col-md-12">
                            {{ Form::label('lead_source', trans('contacts.lead-source').':') }}
                            {{ Form::text('lead_source', old('lead_source'), ['class' => 'form-control', 'placeholder' => trans('contacts.lead-source'), 'required']) }}
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="box-footer">
                        {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
                    </div>
                </div>
            </div>
        </div>
    {{ Form::close() }}

@stop

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}" />
@stop

@section('scripts')
    <script src="{{ asset('js/plugins/select2/select2.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#contact').select2({
                placeholder: '{{trans('contacts.contact-search')}}',
                ajax: {
                    url: '{{route('contacts.ajax.list')}}',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });
        });
    </script>
@stop
