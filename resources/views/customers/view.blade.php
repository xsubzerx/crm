@extends('layouts.template')

@section('title', $title)

@section('content')
    <section class="content invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-user"></i> {{ ($customerData->contact->title && $customerData->contact->title != 'none') ? trans('contacts.titles.'.$customerData->contact->title) .' '.  $customerData->contact->name : $customerData->contact->name }}
                    <span class="text-muted">( {{ trans('contacts.types.'.$customerData->contact->type) }} )</span>
                    <small class="pull-right">Date: {{ $customerData->created_at }}</small>
                </h2>
            </div><!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-6 invoice-col">
                <address>
                    {{ $customerData->contact->address }}<br/>
                    {{  @$customerData->contact->city->name . ', ' . @$customerData->contact->country->name }}<br>
                    <strong>{{ trans('contacts.zip') }}:</strong> {{ $customerData->contact->zip }}<br/>
                    <strong>{{ trans('contacts.mobile') }}:</strong> {{ $customerData->contact->mobile }}<br/>
                    <strong>{{ trans('contacts.phone') }}:</strong> {{ $customerData->contact->phone }}<br/>
                    <strong>{{ trans('contacts.fax') }}:</strong> {{ $customerData->contact->fax }}<br/>
                    <strong>{{ trans('contacts.email') }}:</strong> {{ $customerData->contact->email }}
                </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <address>
                    @if($customerData->contact->type == 'individual')
                        <strong>{{ trans('contacts.position') }}:</strong> {{ $customerData->contact->position }}<br>
                        <strong>{{ trans('contacts.department') }}:</strong> {{ $customerData->contact->department }}<br>
                    @else
                        <strong>{{ trans('contacts.industry') }}:</strong> {{ $customerData->contact->industry }}<br>
                        <strong>{{ trans('contacts.employees') }}:</strong> {{ $customerData->contact->employees }}<br>
                    @endif
                    <strong>{{ trans('contacts.child-of') }}:</strong> <a href="{{route('contacts.show', [@$customerData->contact->parentName->id])}}" target="_blank">{{ @$customerData->contact->parentName->name }}</a><br>
                    <strong>{{ trans('contacts.related') }}:</strong> {{ @$customerData->contact->relatedName->name }}<br>
                    <strong>{{ trans('contacts.created-by') }}:</strong> {{ @$customerData->contact->creator->name }}<br>
                    <strong>{{ trans('contacts.updated-by') }}:</strong> {{ @$customerData->contact->updator->name }}<br>
                    <strong>{{ trans('contacts.website') }}:</strong> <a href="{{@$customerData->contact->website}}" target="_blank">{{ @$customerData->contact->website }}</a><br>
                </address>
            </div><!-- /.col -->
            <div class="col-sm-2 invoice-col">
                <address>
                    <img src="{{ route('image.large', ['contacts',$customerData->contact->image]) }}" class="img-thumbnail" id="pphoto" />
                </address>
            </div>
        </div><!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Qty</th>
                        <th>Product</th>
                        <th>Serial #</th>
                        <th>Description</th>
                        <th>Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Call of Duty</td>
                        <td>455-981-221</td>
                        <td>El snort testosterone trophy driving gloves handsome</td>
                        <td>$64.50</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Need for Speed IV</td>
                        <td>247-925-726</td>
                        <td>Wes Anderson umami biodiesel</td>
                        <td>$50.00</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Monsters DVD</td>
                        <td>735-845-642</td>
                        <td>Terry Richardson helvetica tousled street art master</td>
                        <td>$10.70</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Grown Ups Blue Ray</td>
                        <td>422-568-642</td>
                        <td>Tousled lomo letterpress</td>
                        <td>$25.99</td>
                    </tr>
                    </tbody>
                </table>
            </div><!-- /.col -->
        </div><!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
                <p class="lead">Payment Methods:</p>
                <img src="../../img/credit/visa.png" alt="Visa"/>
                <img src="../../img/credit/mastercard.png" alt="Mastercard"/>
                <img src="../../img/credit/american-express.png" alt="American Express"/>
                <img src="../../img/credit/paypal2.png" alt="Paypal"/>
                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                    Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                </p>
            </div><!-- /.col -->
            <div class="col-xs-6">
                <p class="lead">Amount Due 2/22/2014</p>
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th style="width:50%">Subtotal:</th>
                            <td>$250.30</td>
                        </tr>
                        <tr>
                            <th>Tax (9.3%)</th>
                            <td>$10.34</td>
                        </tr>
                        <tr>
                            <th>Shipping:</th>
                            <td>$5.80</td>
                        </tr>
                        <tr>
                            <th>Total:</th>
                            <td>$265.24</td>
                        </tr>
                    </table>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->

        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                <button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
                <button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment</button>
                <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
            </div>
        </div>
    </section><!-- /.content -->


    <div id="myModal" class="modal">
        <span class="close">&times;</span>
        <img class="modal-content" id="img01">
    </div>
@stop

@section('scripts')
    <script>
        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var img = document.getElementById('pphoto');
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        img.onclick = function(){
            modal.style.display = "block";
            modalImg.src = this.src;
        }

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }
    </script>
@stop