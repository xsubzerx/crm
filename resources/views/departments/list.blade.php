@extends('layouts.template')

@section('title'){{ trans('departments.departments-list') }} @stop

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="text-right">
                <a href="{{ route('departments.create') }}" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-users"></i> {{ trans('common.create-new', ['item' => trans('departments.department')]) }}</a>
            </div>
            <div class="box-header">
                @if(Session::has('msg'))
                    {!! Session::get('msg') !!}
                @endif
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="example1" class="table table-bordered table-striped text-center">
                    <thead>
                        <tr>
                            <th>{{ trans('departments.title') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('common.update') }}</th>
                            <th>{{ trans('common.delete') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($allDepartments as $department)
                        <tr>
                            <td>{{ $department->title }}</td>
                            <td><img src="{{ asset('img/status_'.$department->status.'.png') }}" /></td>
                            <td><a href="{{ route('departments.edit', [$department->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                            <td><button type="button" class="btn btn-danger btn-sm deleteItem" data-uid="{{$department->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>{{ trans('departments.title') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('common.update') }}</th>
                            <th>{{ trans('common.delete') }}</th>
                        </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

@stop

@section('scripts')
    {{ Html::style('css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('js/plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('js/plugins/datatables/dataTables.bootstrap.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();

            $('.deleteItem').on('click', function (e) {
                e.preventDefault();
                var thisBtn = $(this);
                if(confirm('{{trans('common.delete-confirm')}}')) {
                    var url = '{{route('departments.destroy', ":id")}}';
                    $.ajax({
                        url: url.replace(':id', thisBtn.attr('data-id')),
                        type: 'DELETE',
                        contentType:'application/json',
                        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
                    }).done(function () {
                        thisBtn.parents('tr').hide('slow', function(){ thisBtn.parents('tr').remove(); });
                    });
                }
            });
        });
    </script>
@stop