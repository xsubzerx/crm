@extends('layouts.template')

@section('title', $title)

@section('content')

    {{ (isset($departmentData)) ? Form::model($departmentData, ['url' => route('departments.update', [$departmentData->id]), 'method' => 'PUT']) : Form::open(['url' => route('departments.store')]) }}
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    @if(Session::has('msg'))
                        {!! Session::get('msg') !!}
                    @endif
                    @if(!empty($errors->all()))
                        <ul class="callout callout-danger">
                            @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                        </ul>
                    @endif
                </div>
                <div class="box-body">
                    <div class="form-group col-md-12">
                        {{ Form::label('title', trans('departments.title').':') }}
                        {{ Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => trans('departments.title')]) }}
                    </div>
                    <div class="form-group col-md-12">
                        {{ Form::label('status', trans('common.status').':') }}
                        {{ Form::select('status', ['1' => trans('common.active'),'0' => trans('common.deactive')], old('status'), ['id' => 'status', 'class' => 'form-control']) }}
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="box-footer">
                    {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

@stop