@extends('layouts.template')
@section('title', trans('settings.settings'))

@section('content')

    <div class="row">
        <!-- left column -->
        {{ Form::model($settings) }}
        <div class="col-md-12">
            @if(Session::has('msg')){!! Session::get('msg') !!}@endif
            @if(!empty($errors->all()))
                <ul class="callout callout-danger">
                    @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                </ul>
            @endif
            <div class="box-body">
                <div class="form-group col-md-12">
                    {{ Form::label('site_name', trans('settings.site-name').':') }}
                    {{ Form::text('site_name', old('site_name'), array('class' => 'form-control', 'placeholder' => trans('settings.site-name'), 'required')) }}
                </div>
                <div class="form-group col-md-12">
                    {{ Form::label('site_email', trans('settings.site-email').':') }}
                    {{ Form::text('site_email', old('site_email'), array('class' => 'form-control', 'placeholder' => trans('settings.site-email'))) }}
                </div>
                <div class="form-group col-md-12">
                    <div class="col-xs-6 no-padding">
                        {{ Form::label('logo', trans('settings.logo').':') }}
                        {{ Form::file('site_logo', array('class' => 'form-control', 'placeholder' => trans('settings.logo'))) }}
                    </div>
                    <div class="col-xs-6">
                        <img src="{{ (isset($settings) && $settings->site_logo) ? route('image.small', ['',$settings->site_logo]) : asset('img/no-image-found.jpg') }}" class="center-block" height="100" />
                    </div>
                </div>
                <div class="clearfix"></div>
                <br />
            </div>
            <div class="box-footer">
                {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
            </div>
        </div>
        {{ Form::close() }}
    </div>
@stop