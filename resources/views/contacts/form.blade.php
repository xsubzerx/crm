@extends('layouts.template')

@section('title', $title)

@section('content')

    {{ (isset($contactData)) ? Form::model($contactData, ['url' => route('contacts.update', [$contactData->id]), 'method' => 'PUT', 'files'=> true]) : Form::open(['url' => route('contacts.store'), 'files'=> true]) }}
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        @if(Session::has('msg'))
                            {!! Session::get('msg') !!}
                        @endif
                        @if(!empty($errors->all()))
                            <ul class="callout callout-danger">
                                @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                            </ul>
                        @endif
                    </div>
                    <div class="box-body">
                        <div class="form-group col-md-2">
                            {{ Form::label('title', trans('contacts.titles.title').':') }}
                            {{ Form::select('title', ['none' => trans('contacts.titles.none'), 'mr' => trans('contacts.titles.mr'), 'ms' => trans('contacts.titles.ms'), 'mrs' => trans('contacts.titles.mrs'), 'dr' => trans('contacts.titles.dr'), 'prof' => trans('contacts.titles.prof')], old('title'), ['id' => 'title', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group col-md-10">
                            {{ Form::label('name', trans('contacts.name').':') }}
                            {{ Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('contacts.name'), 'required']) }}
                        </div>
                        <div class="form-group col-md-12">
                            {{ Form::label('address', trans('contacts.address').':') }}
                            {{ Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => trans('contacts.address')]) }}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('country', trans('countries.country-title').':') }}
                            {{ Form::select('country_id', $countries, old('country_id'), ['id' => 'country', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('city', trans('cities.city-title').':') }}
                            {{ Form::select('city_id', [], old('city_id'), ['id' => 'city', 'class' => 'form-control']) }}
                            {{ Form::hidden('currCity', old('city_id')) }}
                        </div>
                        <div class="form-group col-md-12">
                            {{ Form::label('email', trans('contacts.email').':') }}
                            {{ Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => trans('contacts.email'), 'required']) }}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('mobile', trans('contacts.mobile').':') }}
                            {{ Form::text('mobile', old('mobile'), ['class' => 'form-control', 'placeholder' => trans('contacts.mobile'), 'required']) }}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('phone', trans('contacts.phone').':') }}
                            {{ Form::text('phone', old('phone'), ['class' => 'form-control', 'placeholder' => trans('contacts.phone')]) }}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('fax', trans('contacts.fax').':') }}
                            {{ Form::text('fax', old('fax'), ['class' => 'form-control', 'placeholder' => trans('contacts.fax')]) }}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('zip', trans('contacts.zip').':') }}
                            {{ Form::text('zip', old('zip'), ['class' => 'form-control', 'placeholder' => trans('contacts.zip')]) }}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('type', trans('contacts.types.type').':') }}
                            {{ Form::select('type', ['individual' => trans('contacts.types.individual'), 'company' => trans('contacts.types.company')], old('type'), ['id' => 'type', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('parent', trans('contacts.parent').':') }}
                            {{ Form::select('parent', ['0' => 'None']+$parents, old('parent'), ['id' => 'parent', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group col-md-6 individual">
                            {{ Form::label('position', trans('contacts.position').':') }}
                            {{ Form::text('position', old('position'), ['class' => 'form-control', 'placeholder' => trans('contacts.position')]) }}
                        </div>
                        <div class="form-group col-md-6 individual">
                            {{ Form::label('department', trans('contacts.department').':') }}
                            {{ Form::text('department', old('department'), ['class' => 'form-control', 'placeholder' => trans('contacts.department')]) }}
                        </div>
                        <div class="form-group col-md-6 company">
                            {{ Form::label('industry', trans('contacts.industry').':') }}
                            {{ Form::text('industry', old('industry'), ['class' => 'form-control', 'placeholder' => trans('contacts.industry')]) }}
                        </div>
                        <div class="form-group col-md-6 company">
                            {{ Form::label('employees', trans('contacts.employees').':') }}
                            {{ Form::number('employees', old('employees'), ['class' => 'form-control', 'placeholder' => trans('contacts.employees')]) }}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('website', trans('contacts.website').':') }}
                            {{ Form::text('website', old('website'), ['class' => 'form-control', 'placeholder' => trans('contacts.website')]) }}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('related', trans('contacts.related').':') }}
                            {{ Form::select('related', ['0' => 'None']+$users, old('related'), ['id' => 'related', 'class' => 'form-control']) }}
                        </div>
                        <div class="form-group col-md-6">
                            {{ Form::label('image', trans('contacts.image').':') }}
                            {{ Form::file('image', array('class' => 'form-control', 'placeholder' => trans('contacts.image'))) }}
                        </div>
                        <div class="form-group col-md-6">
                            <img src="{{ (isset($contactData) && $contactData->image) ? route('image.small', ['contacts',$contactData->image]) : asset('img/no-image-found.jpg') }}" class="center-block" height="100" />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="box-footer">
                        {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
                    </div>
                </div>
            </div>
        </div>
    {{ Form::close() }}

@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            $('.company').hide('fast');
            $('select[name="type"]').on('change', function() {
                if ($(this).val() == 'individual') {
                    $('.company').fadeOut('slow');
                    $('.individual').fadeIn('slow');
                }else if($(this).val() == 'company') {
                    $('.individual').fadeOut('slow');
                    $('.company').fadeIn('slow');
                }
            });

            var country = $('select[name="country_id"]').val();
            var currCity = $('input[name="currCity"]').val();
            getCitiesList(country, currCity);

            $('select[name="country_id"]').on('change', function () {
                country = $('select[name="country_id"]').val();
                getCitiesList(country, currCity);
            });

            function getCitiesList(country, currcity) {
                $.get('{{route('cities.ajax.list', [''])}}/'+country+'/'+currcity, function (data) {
                    $('select[name="city_id"]').html(data);
                });
            }
        });
    </script>
@stop
