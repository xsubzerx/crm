@extends('layouts.template')

@section('title', trans('contacts.contact'))

@section('content')
    <section class="content invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-user"></i> {{ ($contactData->title && $contactData->title != 'none') ? trans('contacts.titles.'.$contactData->title) .' '.  $contactData->name : $contactData->name }}
                    <span class="text-muted">( {{ trans('contacts.types.'.$contactData->type) }} )</span>
                    <small class="pull-right">Date: {{ $contactData->created_at }}</small>
                </h2>
            </div><!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-6 invoice-col">
                <address>
                    {{ $contactData->address }}<br/>
                    {{  @$contactData->city->name . ', ' . @$contactData->country->name }}<br>
                    <strong>{{ trans('contacts.zip') }}:</strong> {{ $contactData->zip }}<br/>
                    <strong>{{ trans('contacts.mobile') }}:</strong> {{ $contactData->mobile }}<br/>
                    <strong>{{ trans('contacts.phone') }}:</strong> {{ $contactData->phone }}<br/>
                    <strong>{{ trans('contacts.fax') }}:</strong> {{ $contactData->fax }}<br/>
                    <strong>{{ trans('contacts.email') }}:</strong> {{ $contactData->email }}
                </address>
            </div><!-- /.col -->
            <div class="col-sm-6 invoice-col">
                <address>
                    @if($contactData->type == 'individual')
                        <strong>{{ trans('contacts.position') }}:</strong> {{ $contactData->position }}<br>
                        <strong>{{ trans('contacts.department') }}:</strong> {{ $contactData->department }}<br>
                    @else
                        <strong>{{ trans('contacts.industry') }}:</strong> {{ $contactData->industry }}<br>
                        <strong>{{ trans('contacts.employees') }}:</strong> {{ $contactData->employees }}<br>
                    @endif
                    <strong>{{ trans('contacts.child-of') }}:</strong> <a href="{{route('contacts.show', [@$contactData->parentName->id])}}" target="_blank">{{ @$contactData->parentName->name }}</a><br>
                    <strong>{{ trans('contacts.related') }}:</strong> {{ @$contactData->relatedName->name }}<br>
                    <strong>{{ trans('contacts.created-by') }}:</strong> {{ @$contactData->creator->name }}<br>
                    <strong>{{ trans('contacts.updated-by') }}:</strong> {{ @$contactData->updator->name }}<br>
                </address>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@stop
