@extends('layouts.template')

@section('title'){{ trans('roles.roles-list') }} @stop

@section('content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="text-right">
                <a href="{{ route('roles.create') }}" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-key"></i> {{ trans('common.create-new', ['item' => trans('roles.role')]) }}</a>
            </div>
            <div class="box-header">
                @if(Session::has('msg'))
                    {!! Session::get('msg') !!}
                @endif
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="example1" class="table table-bordered table-striped text-center">
                    <thead>
                        <tr>
                            <th>{{ trans('roles.name') }}</th>
                            <th>{{ trans('common.update') }}</th>
                            <th>{{ trans('common.delete') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($allRoles as $role)
                        <tr>
                            <td>{{ $role->name }}</td>
                            <td><a href="{{ route('roles.edit', [$role->id]) }}"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                            <td>@if($role->id > 1)<button type="button" class="btn btn-danger btn-sm deleteItem" data-id="{{$role->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i></button>@endif</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>{{ trans('roles.name') }}</th>
                            <th>{{ trans('common.update') }}</th>
                            <th>{{ trans('common.delete') }}</th>
                        </tr>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

@stop

@section('scripts')
    {{ Html::style('css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('js/plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('js/plugins/datatables/dataTables.bootstrap.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();

            var url = '{{route('roles.destroy', ":id")}}';
            $('.deleteItem').on('click', function (e) {
                e.preventDefault();
                var thisBtn = $(this);
                if(confirm('{{trans('common.delete-confirm')}}')) {
                    $.ajax({
                        url: url.replace(':id', thisBtn.attr('data-id')),
                        type: 'DELETE',
                        contentType:'application/json',
                        headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
                    }).done(function () {
                        thisBtn.parents('tr').hide('slow', function(){ thisBtn.parents('tr').remove(); });
                    });
                }
            });
        });
    </script>
@stop