@extends('layouts.template')

@section('title', $title)

@section('content')

    {{ (isset($roleData)) ? Form::model($roleData, ['url' => route('roles.update', [$roleData->id]), 'method' => 'PUT']) : Form::open(['url' => route('roles.store')]) }}
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    @if(Session::has('msg'))
                        {!! Session::get('msg') !!}
                    @endif
                    @if(!empty($errors->all()))
                        <ul class="callout callout-danger">
                            @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                        </ul>
                    @endif
                </div>
                <div class="box-body">
                    <div class="form-group col-md-12">
                        {{ Form::label('name', trans('roles.name').':') }}
                        {{ Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('roles.name')]) }}
                    </div>
                    <div class="form-group col-md-12">
                        {{ Form::label('description', trans('roles.description').':') }}
                        {{ Form::textarea('description', old('description'), ['id' => 'description', 'class' => 'form-control', 'placeholder' => trans('roles.description'), 'rows' => '3']) }}
                    </div>
                    <div class="form-group col-md-12">
                        {{ Form::label('permissions', trans('roles.privileges').':') }}
                        <?php /*<table class="table table-bordered text-center">
                            <tr>
                                <th>{{trans('roles.actions.action')}}</th>
                                <th>{{trans('roles.actions.view')}}</th>
                                <th>{{trans('roles.actions.new')}}</th>
                                <th>{{trans('roles.actions.edit')}}</th>
                                <th>{{trans('roles.actions.destroy')}}</th>
                            </tr>
                            @foreach(config('privileges') as $name=>$privilage)
                                <tr>
                                    <td>{{ trans($name) }}</td>
                                    <td>@if(in_array('view', $privilage)){{ Form::checkbox('view[]', substr(strrchr($name, '.'), 1), (is_array(json_decode($roleData->view)) && in_array(substr(strrchr($name, '.'), 1), json_decode($roleData->view)))) }}@endif</td>
                                    <td>@if(in_array('new', $privilage)){{ Form::checkbox('new[]', substr(strrchr($name, '.'), 1), (is_array(json_decode($roleData->new)) && in_array(substr(strrchr($name, '.'), 1), json_decode($roleData->new)))) }}@endif</td>
                                    <td>@if(in_array('edit', $privilage)){{ Form::checkbox('edit[]', substr(strrchr($name, '.'), 1), (is_array(json_decode($roleData->edit)) && in_array(substr(strrchr($name, '.'), 1), json_decode($roleData->edit)))) }}@endif</td>
                                    <td>@if(in_array('destroy', $privilage)){{ Form::checkbox('destroy[]', substr(strrchr($name, '.'), 1), (is_array(json_decode($roleData->destroy)) && in_array(substr(strrchr($name, '.'), 1), json_decode($roleData->destroy)))) }}@endif</td>
                                </tr>
                            @endforeach
                        </table>
                        <hr/>*/?>
                        <table class="table table-bordered text-center">
                            <tr>
                                <th>{{trans('roles.actions.action')}}</th>
                                <th>None</th>
                                <th>Read Only</th>
                                <th>Read & Write</th>
                            </tr>
                            @foreach(config('privileges') as $name)
                                <tr>
                                    <td>{{ trans($name) }}</td>
                                    <td>{{ Form::radio(substr(strrchr($name, '.'), 1), '0', ($roleData->privilegesDecoded(substr(strrchr($name, '.'), 1)) == '0') ) }}</td>
                                    <td>{{ Form::radio(substr(strrchr($name, '.'), 1), '1', ($roleData->privilegesDecoded(substr(strrchr($name, '.'), 1)) == '1') ) }}</td>
                                    <td>{{ Form::radio(substr(strrchr($name, '.'), 1), '2', ($roleData->privilegesDecoded(substr(strrchr($name, '.'), 1)) == '2') ) }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="box-footer">
                    {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

@stop