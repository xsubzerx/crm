<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(array('prefix' => 'image/{dir}/'), function() {
    Route::get('{filename}', 'Controller@getImage')->name('image');
    Route::get('large-{filename}', 'Controller@getImage')->name('image.large');
    Route::get('medium-{filename}', 'Controller@getImage')->name('image.medium');
    Route::get('small-{filename}', 'Controller@getImage')->name('image.small');
    Route::get('thumbnail-{filename}', 'Controller@getImage')->name('image.thumbnail');
});

Route::group(['prefix' => LaravelLocalization::setLocale()], function (){

    Auth::routes();

    Route::group(['middleware' => ['auth','privileges']], function (){
        Route::get('getCitiesAjaxList/{country}/{city?}', 'Controller@getCitiesAjaxList')->name('cities.ajax.list');

        /******************* Start Opportunities ******************************/
        Route::match(['get','post'], 'opportunities/create', 'ContactsController@createOpportunity')->name('contacts.opportunities.create');
        Route::match(['get','post'], 'opportunities/update/{id}', 'ContactsController@updateOpportunity')->name('contacts.opportunities.update');
        Route::get('opportunities', 'ContactsController@opportunities')->name('contacts.opportunities.list');
        /******************* End Opportunities ******************************/

        Route::resource('departments', 'DepartmentsController');

        Route::get('leads', 'CustomersController@leads')->name('leads.index');
        Route::get('opportunities', 'CustomersController@opportunities')->name('opportunities.index');
        Route::get('dead', 'CustomersController@dead')->name('dead.index');
        Route::get('lost', 'CustomersController@lost')->name('lost.index');
        Route::resource('customers', 'CustomersController');

        Route::resource('contacts', 'ContactsController')->except(['destroy']);
        Route::get('contacts/getContactsAjax', 'ContactsController@getContactsAjax')->name('contacts.ajax.list');

        Route::resource('users', 'UsersController');
        Route::resource('roles', 'RolesController')->except('show');
        Route::match(['get','post'], 'settings', 'HomeController@settings')->name('settings');
        Route::get('/', 'HomeController@index')->name('home');
    });
});
