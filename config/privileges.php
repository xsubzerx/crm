<?php

return [
    'settings.settings',
    'users.users',
    'roles.roles',
    'departments.departments',
    'contacts.contacts',
    'contacts.customers',
    'quotations.quotations'
];

/*
 * [
    'settings.settings'         => ['edit'],
    'users.users'               => ['view', 'new', 'edit', 'destroy'],
    'roles.roles'               => ['view', 'new', 'edit', 'destroy'],
    'departments.departments'   => ['view', 'new', 'edit', 'destroy'],
    'contacts.contacts'         => ['view', 'new', 'edit'],
    'contacts.customers'        => ['view', 'new', 'edit'],
    'quotations.quotations'     => ['view', 'new', 'edit'],
];
 */