<?php

function upload($request, $dir){
    $destinationPath = storage_path('/upload/'.$dir);
    $extension = $request->file('image')->getClientOriginalExtension();
    $fileName = rand(11111,99999).'.'.$extension;
    $request->file('image')->move($destinationPath, $fileName);
    \Image::make($destinationPath."/".$fileName)->resize(150, 150)->blur()->save($destinationPath."/thumbnail-".$fileName);
    \Image::make($destinationPath."/".$fileName)->resize(250, 250)->blur()->save($destinationPath."/small-".$fileName);
    \Image::make($destinationPath."/".$fileName)->resize(400, 350)->blur()->save($destinationPath."/medium-".$fileName);
    \Image::make($destinationPath."/".$fileName)->resize(800, 600)->blur()->save($destinationPath."/large-".$fileName);
    return $fileName;
}