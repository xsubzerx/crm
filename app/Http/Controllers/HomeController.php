<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('dashboard');
    }

    public function settings(Request $request)
    {
        if($request->isMethod('post')){
            $this->validate($request, Setting::$rules);

            $settings             = Setting::whereLang(app()->getLocale())->first();
            if(!$settings){
                $settings = new Setting();
            }
            $settings->site_name  = $request->site_name;
            $settings->site_email = $request->site_email;
            $settings->status     = 1;
            $settings->lang       = app()->getLocale();
            if ($request->hasFile('site_logo')) {
                $settings->site_logo = upload($request, '');
            }
            if($settings->save())
                return back()->with('msg', trans('common.update-success'));
            else
                return back()->with('msg', trans('common.update-failed'));
        }
        $settings = Setting::whereLang(app()->getLocale())->first();
        return view('settings.form', ['settings' => $settings]);
    }
}
