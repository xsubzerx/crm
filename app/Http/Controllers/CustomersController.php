<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Customer;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
    protected $viewsPath = 'customers.';
    protected $data = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($status = 'customer')
    {
        if(\Request::ajax()){
            $allCustomers = Customer::whereStatus($status)->latest()->skip(\Request::get('start'))->take(\Request::get('length'));
            if(\Request::has('search')){
                $allCustomers->whereHas('contact', function($q){
                    $q->where('name', 'like', '%'.\Request::get('search')['value'].'%')
                      ->orWhere('mobile', 'like', '%'.\Request::get('search')['value'].'%')
                      ->orWhere('email', 'like', '%'.\Request::get('search')['value'].'%');
                });
            }
            $allCustomers = $allCustomers->get();

            $data = [];
            if(count($allCustomers)){
                foreach ($allCustomers as $customer){
                    $data[] = [
                        ($customer->contact->title && $customer->contact->title != 'none') ? trans('contacts.titles.'.$customer->contact->title) .' '.  $customer->contact->name : $customer->contact->name,
                        $customer->contact->mobile,
                        $customer->contact->email,
                        '<img src="'.route("image.thumbnail", ["contacts",$customer->contact->image]).'" height="60" />',
                        trans('contacts.types.'.$customer->contact->type),
                        '<a href="'.route("customers.show", [$customer->id]).'"><i class="fa fa-eye" aria-hidden="true"></i></a>'
                    ];
                }
            }
            $this->data = [
                'data'              => $data,
                'draw'              => \Request::get('draw'),
                'recordsTotal'      => Customer::count(),
                'recordsFiltered'   => Customer::count()
            ];
            return response($this->data, 200);
        }
        $this->data['title'] = trans('contacts.customers');
        if($status == 'lead'){
            $this->data['title'] = trans('contacts.leads');
        }elseif($status == 'opportunity'){
            $this->data['title'] = trans('contacts.opportunities');
        }elseif($status == 'dead'){
            $this->data['title'] = trans('contacts.dead');
        }elseif($status == 'lost'){
            $this->data['title'] = trans('contacts.lost');
        }

        return view($this->viewsPath.'list', $this->data);
    }

    public function leads()
    {
        return $this->index('lead');
    }

    public function opportunities()
    {
        return $this->index('opportunity');
    }

    public function dead()
    {
        return $this->index('dead');
    }

    public function lost()
    {
        return $this->index('lost');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['contacts'] = Contact::take(10)->orderBy('name', 'ASC')->pluck('name','id')->all();
        return view($this->viewsPath.'form', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['contact_id' => 'required', 'lead_source' => 'required|max:255']);

        $inputs = $request->only(['contact_id', 'lead_source']);
        $inputs['status'] = 'lead';
        $inputs['created_by'] = \Auth::user()->id;
        $lead = new Customer($inputs);
        if($lead->save()){
            return back()->with('msg', trans('common.add-success'));
        }else{
            return back()->with('msg', trans('common.add-failed'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->data['customerData'] = $customerData = Customer::findOrFail($id);
        $this->data['title'] = ($customerData->contact->title && $customerData->contact->title != 'none') ? trans('contacts.titles.'.$customerData->contact->title) .' '.  $customerData->contact->name : $customerData->contact->name;
        $this->data['breadcrumb'][] = [
            'label' => trans('contacts.leads'),
            'link'  => route('leads.index'),
            'icon'  => 'fa-users'
        ];
        return view($this->viewsPath.'view', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
