<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Country;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        view()->share('locale', app()->getLocale());
        view()->share('config', \App\Models\Setting::find(1));
        view()->share('countries', Country::orderBy('name', 'ASC')->pluck('name', 'id')->all());
    }

    public function getCitiesAjaxList($country, $city = '')
    {
        $cities = City::where('country_id', $country)->orderBy('name', 'ASC')->get();
        $citiesView = \View::make('layouts.partials._cities', ['cities' => $cities, 'currCity' => $city]);
        return response($citiesView->render(), 200);
    }

    function getImage($dir, $filename)
    {
        $path = storage_path('upload/'.$dir.'/'.$filename);
        if(!\File::exists($path)){
            $path = 'assets/img/no-image-found.jpg';
            //abort(404);
        }
        $file = \File::get($path);
        $type = \File::mimeType($path);
        return \Response::make($file, 200)->header("Content-Type", $type);
    }
}
