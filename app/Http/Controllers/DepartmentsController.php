<?php

namespace App\Http\Controllers;

use App\Models\Department;
use Illuminate\Http\Request;

class DepartmentsController extends Controller
{
    protected $viewsPath = 'departments.';
    protected $data = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['allDepartments'] = Department::all();
        return view($this->viewsPath.'list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = trans('departments.create');
        return view($this->viewsPath.'form', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Department::$rules);

        $department         = new Department;
        $department->title  = $request->title;
        $department->status = $request->status;
        if($department->save()){
            return back()->with('msg', trans('common.add-success'));
        }else{
            return back()->with('msg', trans('common.add-failed'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['departmentData'] = $departmentData = Department::find($id);
        if(!$departmentData)
            return back();
        $this->data['title'] = trans('departments.update'). ': '.$departmentData->title;
        return view($this->viewsPath.'form', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Department::$rules);

        $department         = Department::find($id);
        $department->title  = $request->title;
        $department->status = $request->status;
        if($department->save()){
            return back()->with('msg', trans('common.update-success'));
        }else{
            return back()->with('msg', trans('common.update-failed'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Department::destroy($id)){
            if(Request::ajax())
                return response(['msg' => trans('common.delete-success')], 200);
            return back()->with('msg', trans('common.delete-success'));
        }else{
            if(Request::ajax())
                return response(['msg' => trans('common.delete-failed')], 200);
            return back()->with('msg', trans('common.delete-failed'));
        }
    }
}
