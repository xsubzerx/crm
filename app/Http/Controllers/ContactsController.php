<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Department;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContactsController extends Controller
{
    protected $viewsPath = 'contacts.';
    protected $data = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if(\Request::ajax()){
            $allContacts = Contact::latest()->skip(\Request::get('start'))->take(\Request::get('length'));
            if(\Request::has('search')){
                $allContacts->where('name', 'like', '%'.\Request::get('search')['value'].'%')
                            ->orWhere('mobile', 'like', '%'.\Request::get('search')['value'].'%')
                            ->orWhere('email', 'like', '%'.\Request::get('search')['value'].'%');
            }
            $allContacts = $allContacts->get();
            $data = [];
            if(count($allContacts)){
                foreach ($allContacts as $contact){
                    $data[] = [
                        ($contact->title && $contact->title != 'none') ? trans('contacts.titles.'.$contact->title) .' '.  $contact->name : $contact->name,
                        $contact->mobile,
                        $contact->email,
                        '<img src="'.route("image.thumbnail", ["contacts",$contact->image]).'" height="60" />',
                        trans('contacts.types.'.$contact->type),
                        '<a href="'.route("contacts.edit", [$contact->id]).'"><i class="fa fa-pencil" aria-hidden="true"></i></a>'
                    ];
                }
            }
            $this->data = [
                'data'              => $data,
                'draw'              => \Request::get('draw'),
                'recordsTotal'      => Contact::count(),
                'recordsFiltered'   => Contact::count()
            ];
            return response($this->data, 200);
        }
        return view($this->viewsPath.'list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['users'] = User::pluck('name', 'id')->all();
        $this->data['parents'] = Contact::whereParent(0)->orderBy('name', 'ASC')->pluck('name', 'id')->all();
        $this->data['title'] = trans('common.create-new', ['item' => trans('contacts.contact')]);
        return view($this->viewsPath.'form', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Contact::rules());

        $inputs = $request->only([
            'parent', 'name', 'address', 'country_id', 'city_id', 'zip', 'phone', 'mobile', 'fax', 'email', 'website',
            'image', 'title', 'type', 'position', 'department', 'industry', 'employees', 'related'
        ]);
        if($request->hasFile('image')){
            $inputs['image'] = upload($request, 'contacts');
        }
        $inputs['created_by'] = \Auth::user()->id;
        $contact = new Contact($inputs);
        if($contact->save()){
            return back()->with('msg', trans('common.add-success'));
        }else{
            return back()->with('msg', trans('common.add-failed'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->data['contactData'] = Contact::findOrFail($id);
        $this->data['breadcrumb'][] = [
            'label' => trans('contacts.contacts'),
            'link'  => route('contacts.index'),
            'icon'  => 'fa-users'
        ];
        return view($this->viewsPath.'view', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['contactData'] = $contactData = Contact::findOrFail($id);

        $this->data['users'] = User::pluck('name', 'id')->all();
        $this->data['parents'] = Contact::whereParent(0)->orderBy('name', 'ASC')->pluck('name', 'id')->all();
        $this->data['title'] = trans('common.update-item', ['item' => trans('contacts.contact'). ': '. $contactData->name]);
        return view($this->viewsPath.'form', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Contact::rules($id));

        $contact = Contact::find($id);
        $inputs = $request->only([
            'parent', 'name', 'address', 'country_id', 'city_id', 'zip', 'phone', 'mobile', 'fax', 'email', 'website',
            'image', 'title', 'type', 'position', 'department', 'industry', 'employees', 'related'
        ]);
        if($request->hasFile('image')){
            $inputs['image'] = upload($inputs['image'], 'contacts');
        }
        $inputs['updated_by'] = \Auth::user()->id;
        if($contact->update($inputs)){
            return back()->with('msg', trans('common.update-success'));
        }else{
            return back()->with('msg', trans('common.update-failed'));
        }
    }

    public function getContactsAjax()
    {
        if(\Request::has('q')){
            $contacts = Contact::select('id', 'name AS text')->where('name', 'like', '%'.\Request::get('q').'%')->orderBy('name', 'ASC')->get();
            return response($contacts, 200);
        }
    }

}