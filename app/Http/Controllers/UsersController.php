<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    protected $viewsPath = 'users.';
    protected $data = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['allUsers'] = User::all();
        return view($this->viewsPath.'list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = trans('users.create');
        $this->data['departments'] = ['0' => trans('departments.select-department')] + Department::all()->keyBy('id')->map(function ($itm){return $itm['title'];})->toArray();
        $this->data['managers'] = ['0' => trans('users.select-manager')] + User::select('id', 'name')->get()->keyBy('id')->map(function ($itm){return $itm['name'];})->toArray();
        $this->data['roles'] = Role::pluck('name', 'id')->all();
        return view($this->viewsPath.'form', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, User::rules());

        $user               = new User;
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->password     = $request->password;
        $user->phone        = $request->phone;
        $user->mobile       = $request->mobile;
        $user->fax          = $request->fax;
        $user->department_id= $request->department_id;
        $user->job          = $request->job;
        $user->manager      = $request->manager;
        $user->status       = $request->status;
        if ($request->hasFile('image')) {
            $user->image = upload($request, 'users');
        }
        if($user->save()){
            return back()->with('msg', trans('common.add-success'));
        }else{
            return back()->with('msg', trans('common.add-failed'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['userData'] = $userData = User::findOrFail($id);
        $this->data['title'] = trans('users.update'). ': '.$userData->name;
        $this->data['departments'] = array('0' => trans('departments.select-department')) + Department::all()->keyBy('id')->map(function ($itm){return $itm['title'];})->toArray();
        $this->data['managers'] = ['0' => trans('users.select-manager')] + User::select('id', 'name')->where('id', '!=', $id)->get()->keyBy('id')->map(function ($itm){return $itm['name'];})->toArray();
        $this->data['roles'] = Role::pluck('name', 'id')->all();
        return view($this->viewsPath.'form', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, User::rules($id));

        $user               = User::find($id);
        $user->name         = $request->name;
        $user->phone        = $request->phone;
        $user->mobile       = $request->mobile;
        $user->fax          = $request->fax;
        $user->department_id= $request->department_id;
        $user->job          = $request->job;
        $user->manager      = $request->manager;
        $user->status       = $request->status;
        if(!empty($request->password)) {
            $user->password = $request->password;
        }
        if ($request->hasFile('image')) {
            $user->image = upload($request, 'users');
        }
        if($user->save()){
            return back()->with('msg', trans('common.update-success'));
        }else{
            return back()->with('msg', trans('common.update-failed'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(User::destroy($id)){
            if(\Request::ajax())
                return response(['msg' => trans('common.delete-success')], 200);
            return back()->with('msg', trans('common.delete-success'));
        }else{
            if(\Request::ajax())
                return response(['msg' => trans('common.delete-failed')], 200);
            return back()->with('msg', trans('common.delete-failed'));
        }
    }
}
