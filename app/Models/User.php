<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'mobile', 'fax', 'department_id', 'job_id', 'manager', 'image', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    static function rules($id='')
    {
        $rules = [
            'name'      => 'required|max:255',
            'mobile'    => 'required|unique:users,mobile,'.$id,
        ];

        if($id == ''){
            $rules['email'] = 'required|email|unique:users,email';
            $rules['password'] = 'required|min:3';
        }


        return $rules;
    }

    public function setPasswordAttribute($value)
    {
        return $this->attributes['password'] = \Hash::make($value);
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }
}
