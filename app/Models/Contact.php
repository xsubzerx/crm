<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'parent', 'name', 'address', 'country_id', 'city_id', 'zip', 'phone', 'mobile', 'fax', 'email', 'website',
        'image', 'title', 'type', 'position', 'department', 'industry', 'employees', 'related', 'created_by', 'updated_by'
    ];

    static function rules($id='')
    {
        return [
            'name'      => 'required|max:255',
            'email'     => 'required|email|unique:contacts,email,'.$id,
            'mobile'    => 'required|unique:contacts,mobile,'.$id,
        ];
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function parentName()
    {
        return $this->belongsTo(Contact::class, 'parent');
    }

    public function relatedName()
    {
        return $this->belongsTo(User::class, 'related');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updator()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function leads()
    {
        return $this->hasMany('App\Models\Customer')->whereStatus('lead');
    }

    public function opportunities()
    {
        return $this->hasMany('App\Models\Customer')->whereStatus('opportunity');
    }
}
