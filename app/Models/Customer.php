<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['contact_id', 'lead_source', 'status', 'created_by', 'updated_by'];

    static $rules = ['contact_id' => 'required', 'status' => 'required'];

    public function contact()
    {
        return $this->belongsTo('App\Models\Contact');
    }
}
